import React, { Component } from "react";
import { connect } from "react-redux";
import {THEM_SINH_VIEN, UPDATE_SINH_VIEN} from './redux/constant/constant'


//bài này em đi tham khảo source để làm thêm thanh search 
class FormInput extends Component {
  // tạo state mới lấy dữ liệu trong form truyền lên store
  state = {
    NewSV: [
      {
        id: '',
        hoTen: '',
        phone: '',
        email: '',
      },
    ],
    errors: {
      id: '',
      hoTen: '',
      phone: '',
      email: '',
    },
    valid: false,
    svListCheck: [],
  };

  handleGetInput = (event) => {
    let { value, name, type, pattern } = event.target;
    let errorMsg = '';
    // CHECK ALL INPUT FORMAT
    // CASE: NULL
    if (value.trim() === ''){ // <-- nếu ô input rỗng, ko tính khoảng trắng đầu cuối (trim())
      errorMsg = name + ' không được để trống!';
    }
    // CASE: NOT NULL

    if (value.trim() !== '' && (type === 'email' || type === 'text')){
      if (name === "id"){
        const regex = new RegExp(pattern);
        if (!regex.test(value)){
          errorMsg = name + ' không đúng định dạng hoặc yêu cầu!'
        }
        let idValue = {[name]:value}
        let svListCheck = this.props.svList.map(item => item.id)
        console.log('svListCheck',svListCheck);
        svListCheck.forEach(element => {
          if (Number(element) === Number(idValue.id)){
            errorMsg = name + ' trùng với mã SV đã khởi tạo!'
          }
        })
      }
    
      else{
        const regex = new RegExp(pattern);
        if (!regex.test(value)){
          errorMsg = name + ' không đúng định dạng hoặc yêu cầu!'
        }
      }
    } 
    // update value/error
    let NewSV = { ...this.state.NewSV,[name]:value};
    let errors = {...this.state.errors,[name]:errorMsg}
    // reset state
    this.setState(
      {
        ...this.state,
        NewSV: NewSV,
        errors: errors,
      },
      () => {
        console.log(this.state);
        this.checkValidation();
      }
    );
  };

  checkValidation =()=>{
    let valid = true;
    // Check erros
    for (let item in this.state.errors){
      // nếu như 1 trong các input có lỗi (errorMsg !== '')
      if (this.state.errors[item] !== ''){
        valid = false;
      }
    }
   
    if (valid === true){
      //  nếu valid = true, nhưng giá trị nhập vào của input là rỗng thì vần set valid = false
      for (let item in this.state.NewSV){
        if (this.state.NewSV[item] === ''){
          valid = false;
        }
      }
    }

    this.setState({valid:valid})
  }

  render() {
    return (
      <div>
        <div className="row pb-5">
          <div className="col-12">
            <h2 className="bg-dark text-light py-3 pl-2 text-left">
              Thông Tin Sinh Viên
            </h2>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Mã SV</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^\d+$"
              className="form-control"
              name="id" placeholder="Mã SV"
              value={this.state.NewSV.id || ''}
              required
            />
            <span className="text-danger">{this.state.errors.id}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Họ Tên</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*)[a-zA-Z\s]{5,}$"
              className="form-control"
              name="hoTen" placeholder="Nhập tên không dấu"
              value={this.state.NewSV.hoTen || ''}
              required
            />
            <span className="text-danger">{this.state.errors.hoTen}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Số Diện Thoại</p>
            <input
              onChange={this.handleGetInput}
              type="text" pattern="^(0|\+84)(\s|\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\d)(\s|\.)?(\d{3})(\s|\.)?(\d{3})$"
              className="form-control"
              name="phone" placeholder="SDT"
              value={this.state.NewSV.phone || ''}
              required
            />
            <span className="text-danger">{this.state.errors.phone}</span>
          </div>
          <div className="form-group col-6">
            <p className="text-left" style={{fontWeight: '600'}}>Email</p>
            <input
              onChange={this.handleGetInput}
              type="email" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
              className="form-control"
              name="email" placeholder="EMail"
              value={this.state.NewSV.email || ''}
              required
            />
            <span className="text-danger">{this.state.errors.email}</span>
          </div>
          <div className="col-12 text-right mt-1">

           
            <br />
           
            {this.state.valid ? <button onClick={() => {this.props.handleAddSV(this.state.NewSV);}} className="btn btn-primary py-2 text-left mr-2">Thêm sinh viên</button> : <button onClick={() => {this.props.handleAddSV(this.state.NewSV);}} className="btn btn-primary py-2 text-left mr-2" disabled>Thêm sinh viên</button>}

    
            {this.state.valid ? <button onClick={() => {this.props.handleUpdateSV(this.state.NewSV);}} className="btn btn-warning py-2 text-left">Cập nhật sinh viên</button> : <button onClick={() => {this.props.handleUpdateSV(this.state.NewSV);}} className="btn btn-warning py-2 text-left" disabled>Cập nhật sinh viên</button>}
          </div>
        </div>
      </div>
    );
  }
  componentDidUpdate(prevProps, prevState){
    if (prevProps.svEdited.id !== this.props.svEdited.id){
        this.setState({
            NewSV: this.props.svEdited
        })
    } 

  }
}

let mapStateToProps = (state) => {
  return {
    svList: state.formReducer.svList,
    svEdited: state.formReducer.svEdited,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddSV: (sinhVien) => {
      const action = {
        type: THEM_SINH_VIEN,
        sinhVien,
      };
      dispatch(action);
    },
    handleUpdateSV: (sinhVien) => {
        const action = {
          type: UPDATE_SINH_VIEN,
          sinhVien,
        };
        dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormInput);
