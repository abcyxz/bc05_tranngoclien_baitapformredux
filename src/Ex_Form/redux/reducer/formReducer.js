import {THEM_SINH_VIEN, UPDATE_SINH_VIEN, XOA_SINH_VIEN, EDIT_SINH_VIEN, SEARCH_SINH_VIEN} from '../constant/constant'

const initialState = {
  svList: [
    {
      id: 1,
      hoTen: 'Tran Ngoc Lin',
      phone: '0909123456',
      email: 'tranngoclin@gmail.com',
    },
    {
      id: 2,
      hoTen: 'Lin Ngoc Tran',
      phone: '0909572536',
      email: 'linngoctran@gmail.com',
    },
    {
      id: 3,
      hoTen: 'Ngoc Lin Tran',
      phone: '0795112367',
      email: 'ngoclintran@gmail.com',
    },
    {
      id: 4,
      hoTen: 'Ngoc Tran Lin',
      phone: '0903969696',
      email: 'ngoctranlin@yahoo.com',
    },
  
  ],
  svEdited: {
    id: null,
    hoTen: '',
    phone: '',
    email: '',
  },
  keyword: '',
  isSearch: false,
  svListSearched: [],
}

export const formReducer = (state = initialState, action) => {
  switch (action.type){
    case THEM_SINH_VIEN: {
      state.isSearch = false;
      let svListUpdated = [...state.svList, action.sinhVien];
      console.log('1. sinh vien added: ',action.sinhVien);
      return {...state, svList:svListUpdated}
    }
    case XOA_SINH_VIEN: {
      state.isSearch = false;
      let clonedSVList = [...state.svList];
      clonedSVList.splice(action.index, 1);
      console.log('2. index removed: ',action.index)
      return {...state, svList:clonedSVList}
    }
    case EDIT_SINH_VIEN: {
      state.isSearch = false;
      return {...state, svEdited: action.sinhVien}
    }
    case UPDATE_SINH_VIEN: {
      state.isSearch = false;
      state.svEdited = {...state.svEdited, action}
      // console.log('1. action: ',action);
      // console.log('1. svEdited: ', state.svEdited);
      let svListUpdate = [...state.svList];
      // console.log('2. svListUpdate: ', svListUpdate)
      let index = svListUpdate.findIndex(item => item.id === state.svEdited.id)
      console.log('3. sinh viên edited: ', action.sinhVien)
      // console.log('3. index: ', index);
      if (index !== -1){
        svListUpdate[index] = action.sinhVien;
        // console.log('4. state.svEdited: ', state.svEdited);
        // console.log('5. svListUpdate: ', svListUpdate);
      }
      return {...state, svList:svListUpdate}
    }
    case SEARCH_SINH_VIEN: {
      let svListFiltered = [...state.svList]
      if (action.keyword === ''){
        return {...state, svListSearched: state.svList}
      } else 
      state.isSearch = true;
      if (action.keyword.length > 1){
        let finalList = svListFiltered.filter(keyword => keyword.hoTen.toLowerCase().indexOf(action.keyword) > -1)
        console.log('4. action.keyword: ',action.keyword );
        console.log('5. finalList: ', finalList);
        console.log('isSearched (true/false): ',state.isSearch)
        return {...state, svListSearched: finalList}
      }
    }
    // eslint-disable-next-line no-fallthrough
    default: {
      return state
    }
  }
} 
