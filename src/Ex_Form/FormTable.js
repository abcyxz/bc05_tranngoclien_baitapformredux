import React, { Component } from 'react'
import { connect } from 'react-redux'
import {XOA_SINH_VIEN, EDIT_SINH_VIEN, SEARCH_SINH_VIEN} from './redux/constant/constant'

class FormTable extends Component {

    // khai báo state keyword cho phần search
    constructor(props){
        super(props);
        this.state = {
            keyword: '',  
        }
    }

    renderDanhSachSV = () => {
        return this.props.svList.map((item, index) => {
            return (
                <tr key = {index}>
                    <td>{item.id}</td>
                    <td>{item.hoTen}</td>
                    <td>{item.phone}</td>
                    <td>{item.email}</td>
                    <td className='d-flex justify-content-around'>
                        <div onClick={()=>this.props.handleRemoveSV(index)} style={{color: 'red', cursor: 'pointer', fontSize: 20, textShadow: '1px 1px #00000f'}} title='Xóa sinh viên'><i className="fa fa-trash"></i></div>
                        <div onClick={()=>this.props.handleEditSV(item)} style={{color: 'black', cursor: 'pointer', fontSize: 18, textShadow: '1px 1px #EB6440'}} title='Sửa sinh viên'><i className="fa fa-solid fa-pencil-square"></i></div>
                    </td>
                </tr>
            )
        })
    }

    handleOnChange = (event) => {
        this.setState({
            keyword: event.target.value
        })
    }

    onSearch = () => {
        return this.props.handleSearchSV(this.state.keyword)
    }

    renderDanhSachSVSearched = () => {
        return this.props.svListSearched.map((item, index) => {
            return (
                <tr key = {index}>
                    <td>{item.id}</td>
                    <td>{item.hoTen}</td>
                    <td>{item.phone}</td>
                    <td>{item.email}</td>
                    <td className='d-flex justify-content-around'>
                        <div onClick={()=>this.props.handleRemoveSV(index)} style={{color: 'red', cursor: 'pointer', fontSize: 20, textShadow: '1px 1px #00000f'}} title='Xóa sinh viên'><i className="fa fa-trash"></i></div>
                        <div onClick={()=>this.props.handleEditSV(item)} style={{color: 'black', cursor: 'pointer', fontSize: 18, textShadow: '1px 1px #EB6440'}} title='Sửa sinh viên'><i className="fa fa-solid fa-pencil-square"></i></div>
                    </td>
                </tr>
            )
        })
    }
    
  render() {
    return (
      <div>
        <div className="input-group w-50">
            <input onChange={this.handleOnChange} value={this.state.keyword} type="text" className="form-control mb-3" placeholder="Điền tên SV (Không dấu)"/>
            <div className="input-group-prepend">
                <span onClick={this.onSearch} title="Search sinh viên" className="input-group-text bg-dark mb-2" style={{cursor: 'pointer', fontSize: 20}}><i className="fa fa-search text-light" /></span>
            </div>
        </div>
        <table className='table'>
            <thead className='bg-secondary text-light py-2 text-left'>
                <tr>
                    <th>Mã SV</th>
                    <th>Họ tên</th>
                    <th>Đt</th>
                    <th>Email</th>
                    <th>Xóa/Edit</th>
                </tr>
            </thead>
            {this.props.isSearch ? 
            <tbody className='text-left'>
                {this.renderDanhSachSVSearched()}
            </tbody> :
            <tbody className='text-left'>
                {this.renderDanhSachSV()}
            </tbody>
            }

        </table>
      </div>
    )
  }
}

let mapStateToProps = (state) => {
    return {
        svList: state.formReducer.svList,
        isSearch: state.formReducer.isSearch,
        svListSearched: state.formReducer.svListSearched,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleRemoveSV: (index) => {
            const action = {
                type: XOA_SINH_VIEN,
                index,
            }
            dispatch (action);
        },
        handleEditSV: (sinhVien) => {
            const action = {
                type: EDIT_SINH_VIEN,
                sinhVien,
            }
            dispatch (action);
        },
        handleSearchSV: (keyword) => {
            const action = {
                type: SEARCH_SINH_VIEN,
                keyword,
            }
            dispatch (action);
        },
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(FormTable)