import React, { Component } from 'react'
import FormInput from './FormInput';
import FormTable from './FormTable';


export default class FormStoreReducer extends Component {
  render() {
    return (
      <div className='container py-2 bg-light'>
        <FormInput/>
        <FormTable/>
      </div>
    )
  }
}
